#!/bin/sh
export LDAP_PW=`cat /secrets/ldap_pw`
sed -i "s/%ldap_pw%/$LDAP_PW" /etc/postfix/virtual_alias_maps.cf
sed -i "s/%ldap_pw%/$LDAP_PW" /etc/postfix/virtual_mailbox_domains.cf
sed -i "s/%ldap_pw%/$LDAP_PW" /etc/postfix/virtual_mailbox_maps.cf
