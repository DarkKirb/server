#!/bin/bash

export SROOT_PW=`cat /secrets/root_pw`
export ROOT_PW=`cat /secrets/root_pw_cleartext`

[[ `hostname` =~ -([0-9]+)$ ]] || exit 1
ordinal=${BASH_REMATCH[1]}
ID=$(($ordinal + 1))

rm -rf /ldap/config
mkdir -pv /ldap/db /ldap/config
echo "Loading openldap configuration"
cat /initial.ldif | sed "s/%ROOT_PW%/$SROOT_PW/g" | sed "s/%server_id%/$ID/g" | slapadd -n 0 -F /ldap/config

slapd -d stats -F /ldap/config &
slapd_pid=$!
#RUST_LOG=info watchdog &
#watchdog_pid=$!

trapped() {
    kill -TERM $slapd_pid
#    kill -TERM $watchdog_pid
    exit 0
}

trap trapped TERM
wait $slapd_pid
#wait $watchdog_pid
