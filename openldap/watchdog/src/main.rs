use ldap3::LdapConn;
use ldap3::Mod;
use std::collections::HashSet;
use std::env;
use std::fs::File;
use std::io::prelude::*;
// https://kubernetes.default.svc/api/v1/namespaces/server-12970643/pods
fn main() -> Result<(), Box<dyn std::error::Error>> {
    emoji_logger::init();
    let mut cert_buf = Vec::new();
    File::open("/var/run/secrets/kubernetes.io/serviceaccount/ca.crt")?
        .read_to_end(&mut cert_buf)?;
    let mut token = String::new();
    File::open("/var/run/secrets/kubernetes.io/serviceaccount/token")?
        .read_to_string(&mut token)?;
    let cert = reqwest::Certificate::from_pem(&cert_buf)?;
    let client = reqwest::Client::builder()
        .use_rustls_tls()
        .add_root_certificate(cert)
        .build()?;

    let mut namespace = String::new();
    File::open("/var/run/secrets/kubernetes.io/serviceaccount/namespace")?
        .read_to_string(&mut namespace)?;

    let mut old_vec = Vec::new();

    loop {
        use std::{thread, time};

        let mut current_instances = Vec::new();

        // get pods in namespace
        if let Ok(mut resp) = client
            .get(&format!(
                "https://kubernetes.default.svc/api/v1/namespaces/{}/pods",
                namespace
            ))
            .bearer_auth(&token)
            .send()
        {
            if let Ok(pod_list) = resp.json::<k8s_openapi::api::core::v1::PodList>() {
                let it = pod_list.items.iter().filter(|i| {
                    if let Some(meta) = &i.metadata {
                        if let Some(name) = &meta.name {
                            return name.starts_with("openldap-");
                        }
                    }
                    false
                });
                for i in it {
                    if let Some(meta) = &i.metadata {
                        if let Some(name) = &meta.name {
                            if let Ok(i) = name[9..].parse::<u32>() {
                                current_instances.push(i);
                            }
                        }
                    }
                }
            }
        }

        println!("Found {} instances: ", current_instances.len());
        for no in current_instances.iter() {
            println!(
                "Instance {} at openldap-{}.openldap.{}.svc.cluster.local",
                no + 1,
                no,
                namespace
            );
        }

        if old_vec == current_instances {
            thread::sleep(time::Duration::from_secs(30));
            continue;
        }

        println!("Synchronizing ldap configuration");
        let conn = LdapConn::new("ldap://localhost:389");
        if let Err(e) = conn {
            println!("Failed to connect to ldap: {:?}", e);
            thread::sleep(time::Duration::from_secs(30));
            continue;
        }
        let conn = conn.unwrap();
        old_vec = current_instances.clone();
        let root_pw = env::var("ROOT_PW").unwrap();
        if let Err(e) = conn
            .simple_bind("cn=root,dc=darkkirb,dc=de", &root_pw)
            .and_then(|e| e.success())
        {
            println!("Failed to BIND to ldap: {:?}", e);
            thread::sleep(time::Duration::from_secs(30));
            continue;
        }

        // Set the olcServerIds.
        let mut olcServerId = HashSet::new();
        let mut olcSyncReplConfig = HashSet::new();
        let mut olcSyncReplDatabase = HashSet::new();
        let mut olcMirrorMode = HashSet::new();
        olcMirrorMode.insert("TRUE");
        for no in current_instances.iter() {
            olcServerId.insert(format!(
                "{} ldap://openldap-{}.openldap.{}.svc.cluster.local",
                no + 1,
                no,
                namespace
            ));
            olcSyncReplConfig.insert(format!("rid={:0>3} provider=ldap://openldap-{}.openldap.{}.svc.cluster.local binddn=\"cn=root,dc=darkkirb,dc=de\" bindmethod=simple credentials=\"{}\" searchbase=\"cn=config\" type=refreshAndPersist retry=\"5 5 30 100\" timeout=5",
                                             no + 1,
                                             no,
                                             namespace,
                                             root_pw
            ));
            olcSyncReplDatabase.insert(format!("rid=1{:0>3} provider=ldap://openldap-{}.openldap.{}.svc.cluster.local binddn=\"cn=root,dc=darkkirb,dc=de\" bindmethod=simple credentials=\"{}\" searchbase=\"dc=darkkirb,dc=de\" type=refreshOnly retry=\"5 5 30 100\" timeout=5 interval=00:00:00:10",
                                             no + 1,
                                             no,
                                             namespace,
                                             root_pw
            ));
        }

        if let Err(e) = conn
            .modify(
                "cn=config",
                vec![Mod::Replace("olcServerId".to_string(), olcServerId)],
            )
            .and_then(|r| r.success())
        {
            println!("Failed to replace olcServerId: {:?}", e);
            thread::sleep(time::Duration::from_secs(30));
            continue;
        }

        if let Err(e) = conn
            .modify(
                "olcDatabase={0}config,cn=config",
                vec![Mod::Replace("olcSyncRepl".to_string(), olcSyncReplConfig)],
            )
            .and_then(|r| r.success())
        {
            println!("Failed to replace olcServerId: {:?}", e);
            thread::sleep(time::Duration::from_secs(30));
            continue;
        }
        conn.modify(
            "olcDatabase={0}config,cn=config",
            vec![Mod::Add("olcMirrorMode", olcMirrorMode.clone())],
        )
        .ok();

        if let Err(e) = conn
            .modify(
                "olcDatabase={1}mdb,cn=config",
                vec![Mod::Replace("olcSyncRepl".to_string(), olcSyncReplDatabase)],
            )
            .and_then(|r| r.success())
        {
            println!("Failed to replace olcServerId: {:?}", e);
            thread::sleep(time::Duration::from_secs(30));
            continue;
        }
        conn.modify(
            "olcDatabase={1}mdb,cn=config",
            vec![Mod::Add("olcMirrorMode", olcMirrorMode)],
        )
        .ok();

        conn.unbind().ok();

        thread::sleep(time::Duration::from_secs(30));
    }
}
